package com.javadude.expressions

class Scope(val parent : Scope? = null) {
    private val variables = mutableMapOf<String, Int?>()
    operator fun get(name : String) : Int {
        return variables[name] ?: parent?.get(name) ?: throw IllegalStateException("variable $name not found!!!")
    }
    operator fun set(name : String, value : Int) {
        variables[name] = value
    }
}


abstract class Evaluator<T> {
    abstract fun evaluate(scope : Scope) : T
}

val topScope = Scope()

class Test1 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
//            variables["x"] = 42
//            variables["y"] = 10
//            variables["z"] = 3

//            // 4 * 3 + 6 / 2
//            val expr = AddEvaluator(
//                    MultiplyEvaluator(
//                        LiteralEvaluator(4),
//                        LiteralEvaluator(3)
//                    ),
//                    DivideEvaluator(
//                        LiteralEvaluator(6),
//                        LiteralEvaluator(2)
//                    )
//            )
//            // x * y + z / 2
//            val expr = AddEvaluator(
//                    MultiplyEvaluator(
//                        VariableEvaluator("x"),
//                        VariableEvaluator("y")
//                    ),
//                    DivideEvaluator(
//                        VariableEvaluator("z"),
//                        LiteralEvaluator(2)
//                    )
//            )
//            // x = 42
//            // y = 10
//            // z = 3
//            // x * y + z / 2
//            val expr : Evaluator<Int> =
//                    EvaluatorListEvaluator(
//                        AssignmentEvaluator("x", LiteralEvaluator(42)),
//                        AssignmentEvaluator("y", LiteralEvaluator(10)),
//                        AssignmentEvaluator("z", LiteralEvaluator(3)),
//                        AddEvaluator(
//                            MultiplyEvaluator(
//                                VariableEvaluator("x"),
//                                VariableEvaluator("y")
//                            ),
//                            DivideEvaluator(
//                                VariableEvaluator("z"),
//                                LiteralEvaluator(2)
//                            )
//                    )
//            )
            // x = 42
            // y = 10
            // z = 3
            // x * y + z / 2
            // for(x in 1,2,3,4,5) {
            //    print x * y + z / 2
            //}
            val expr : Evaluator<Int> =
                    EvaluatorListEvaluator(
                        AssignmentEvaluator("x", LiteralEvaluator(42)),
                        AssignmentEvaluator("y", LiteralEvaluator(10)),
                        AssignmentEvaluator("z", LiteralEvaluator(3)),
                        AddEvaluator(
                            MultiplyEvaluator(
                                VariableEvaluator("x"),
                                VariableEvaluator("y")
                            ),
                            DivideEvaluator(
                                VariableEvaluator("z"),
                                LiteralEvaluator(2)
                            )
                        ),
                        ForEachEvaluator("x",
                                ListEvaluator(
                                        LiteralEvaluator(1),
                                        LiteralEvaluator(2),
                                        LiteralEvaluator(3),
                                        LiteralEvaluator(4),
                                        LiteralEvaluator(5)
                                ),
                                PrintEvaluator(
                                        AddEvaluator(
                                                MultiplyEvaluator(
                                                        VariableEvaluator("x"),
                                                        VariableEvaluator("y")
                                                ),
                                                DivideEvaluator(
                                                        VariableEvaluator("z"),
                                                        LiteralEvaluator(2)
                                                )
                                        )
                                )
                        ),
                        LiteralEvaluator(42)
            )

            expr.evaluate(topScope)
        }
    }
}

class EvaluatorListEvaluator<T>(vararg val evaluators : Evaluator<*>) : Evaluator<T>() {
    override fun evaluate(scope : Scope) : T {
        val newScope = Scope(scope)
//        var value : Any? = null
//        evaluators.forEach {
//            value = it.evaluate()
//        }
        evaluators.take(evaluators.size-1).forEach {
            it.evaluate(newScope)
        }
        return evaluators.last().evaluate(newScope) as? T ?: throw IllegalArgumentException("bad type for last evaluator")
    }
}

class ListEvaluator(vararg val values : Evaluator<Int>) : Evaluator<List<Int>>() {
    override fun evaluate(scope :Scope): List<Int> {
        return values.map { it.evaluate(scope) }
    }
}
class PrintEvaluator<T>(val value : Evaluator<T>) : Evaluator<Unit>() {
    override fun evaluate(scope :Scope) {
        println(value.evaluate(scope))
    }
}

class ForEachEvaluator(val name : String,
                       val list : Evaluator<List<Int>>,
                       vararg val evaluators : Evaluator<*>) : Evaluator<Unit>() {
    override fun evaluate(scope :Scope) {
        val newScope = Scope(scope)
        list.evaluate(newScope).forEach {
            newScope[name] = it
            evaluators.forEach {
                it.evaluate(newScope)
            }
        }
    }
}

class AssignmentEvaluator(val name : String, val value : Evaluator<Int>) : Evaluator<Unit>() {
    override fun evaluate(scope :Scope) {
        scope[name] = value.evaluate(scope)
    }
}
class VariableEvaluator(val name : String) : Evaluator<Int>() {
    override fun evaluate(scope :Scope) = scope[name]
}

class LiteralEvaluator<T>(val value : T) : Evaluator<T>() {
    override fun evaluate(scope :Scope) = value
}

class AddEvaluator(val op1 : Evaluator<Int>,
                   val op2 : Evaluator<Int>) : Evaluator<Int>() {
    override fun evaluate(scope :Scope) = op1.evaluate(scope) + op2.evaluate(scope)
}
class SubtractEvaluator(val op1 : Evaluator<Int>,
                        val op2 : Evaluator<Int>) : Evaluator<Int>() {
    override fun evaluate(scope :Scope) = op1.evaluate(scope) - op2.evaluate(scope)
}
class MultiplyEvaluator(val op1 : Evaluator<Int>,
                        val op2 : Evaluator<Int>) : Evaluator<Int>() {
    override fun evaluate(scope :Scope) = op1.evaluate(scope) * op2.evaluate(scope)
}
class DivideEvaluator(val op1 : Evaluator<Int>,
                      val op2 : Evaluator<Int>) : Evaluator<Int>() {
    override fun evaluate(scope :Scope) = op1.evaluate(scope) / op2.evaluate(scope)
}
